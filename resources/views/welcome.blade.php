<!DOCTYPE html>
<html lang="fr">
@include('partials._header')
<body>
  <header id="header">
    @include('partials._nav')
    <section id="hero">
    <div class="container">
      <div class="row d-flex align-items-center">
      <div class="col-lg-6 py-5 py-lg-0 order-2 order-lg-1" data-aos="fade-right">
        <h1>E-SOLUX GROUP</h1>
        <h2>Le numérique à portée des mains</h2>
      </div>

      <div class="col-lg-6 order-1 order-lg-2 hero-img" data-aos="fade-left">
        <img src="assets/img/hero-img.png" class="img-fluid" alt="">
      </div>
    </div>
    </div>
  </section>
  </header>
  <main id="main">
    <!-- ======= About Section ======= -->
    <section id="about" class="about section-bg">
      <div class="container">
        <div class="row">
          <div class="image col-xl-5 d-flex align-items-stretch justify-content-center justify-content-lg-start"></div>
          <div class="col-xl-7 pl-0 pl-lg-5 pr-lg-1 d-flex align-items-stretch">
            <div class="content d-flex flex-column justify-content-center">
              <h3 data-aos="fade-in" data-aos-delay="100" class="text-center mb-4">Qui sommes-nous?</h3>
                <p data-aos="fade-in" class="text-justify">
                    Fondée en 2020, E-SOLUX GROUP est une entreprise de conseil et de développement spécialisée dans les nouvelles technologies.
                    La Team E-SOLUX GROUP est composée de consultants, d’ingénieurs et de développeurs dévoués et expérimentés.
                 Notre métier ne consiste pas uniquement en l’exécution et le développement technique. Nous veillons à accompagner nos clients en amont et en avale,depuis la phase de réflexion jusqu’à la concrétisation de leurs projets,afin d’apporter des solutions à la pointe de la technologie tout en respectant les exigences du marché.</p>
              </div>

            </div><!-- End .content-->
          </div>
        </div>

      </div>
    </section><!-- End About Section -->

    <!-- ======= Services Section ======= -->
    <section id="services" class="services section-bg">
      <div class="container">

        <div class="section-title">
          <h2 data-aos="fade-in">Services</h2>
          <p data-aos="fade-in"> La création de valeur et la réussite de nos clients sont primordiales pour nous.Voila pourquoi nous proposons plusieurs services,entres autres:</p>
        </div>

        <div class="row">
          <div class="col-md-4" data-aos="fade-right">
            <div class="card">
              <div class="card-img">
                <img src="assets/img/services-1.jpg" alt="...">
              </div>
              <div class="card-body">
                <h5 class="card-title"><a href="#">Consulting & Stratégies</a></h5>
                <p class="card-text">Nous vous accompagnons dans l’élaboration de votre stratégie digitale en accord avec votre écosystème marketing et technique</p>
              </div>
            </div>
          </div>
          <div class="col-md-4" data-aos="fade-left">
            <div class="card">
              <div class="card-img">
                <img src="assets/img/services-4.jpg" alt="...">
              </div>
              <div class="card-body">
                <h5 class="card-title"><a href="#">Création  de Sites Web</a></h5>
                <p class="card-text">
                    Nous créons tout type de sites web: institutionnel, vitrine, e-commerce ainsi que des landing pages personnalisées
                   </p>

              </div>
            </div>
          </div>
          <div class="col-md-4" data-aos="fade-right">
            <div class="card">
              <div class="card-img">
                <img src="assets/img/services-3.jpg" alt="...">
              </div>
              <div class="card-body">
                <h5 class="card-title"><a href="#">Business intelligence et data analytics</a></h5>
                <p class="card-text">Nous utilisons du Machine Learning et des algorithmes d’intelligence artificielle pour capitaliser sur la data et faire des prédictions</p>

              </div>
            </div>
          </div>
          <div class="col-md-4" data-aos="fade-left">
            <div class="card">
              <div class="card-img">
                <img src="assets/img/services-2.jpg" alt="...">
              </div>
              <div class="card-body">
                <h5 class="card-title"><a href="#">Création de Logiciels</a></h5>
                <p class="card-text">Nous développons des logiciels performants à la pointe de la technologie, pour la gestion de votre travail au quotidien (TMS,CRM…)</p>

              </div>
            </div>

          </div>


          <div class="col-md-4" data-aos="fade-left">
            <div class="card">
              <div class="card-img">
                <img src="assets/img/services-4.jpg" alt="...">
              </div>
              <div class="card-body">
                <h5 class="card-title"><a href="">Digitalisation des process </a></h5>
                <p class="card-text">
                    Nous vous proposons des solutions digitales innovantes basées sur nos expériences et en adéquation avec les bonnes pratiques internationales
                   </p>

              </div>
            </div>
          </div>
          <div class="col-md-4" data-aos="fade-left">
            <div class="card">
              <div class="card-img">
                <img src="assets/img/services-2.jpg" alt="...">
              </div>
              <div class="card-body">
                <h5 class="card-title text-center"><a href="#">Transformation  digitale</a></h5>
                <p class="card-text ">Nous vous accompagnons dans la réflexion et la réalisation de vos projets digitaux en vous offrant notre large expertise dans le domaine</p>

              </div>
            </div>

          </div>
        </div>

      </div>
    </section><!-- End Services Section -->

    <!-- ======= Features Section ======= -->
    <section id="features" class="features section-bg">
      <div class="container">

        <div class="section-title">
          <h2 data-aos="fade-in">Pourquoi nous choisir?</h2>
        </div>

        <div class="row content">
          <div class="col-md-5" data-aos="fade-right">
            <img src="assets/img/features-1.svg" class="img-fluid" alt="">
          </div>
          <div class="col-md-7 pt-4" data-aos="fade-left">
            <h3>Rigueur de l’analyse</h3>
            <p>
                Nous préparons une base solide avant de bâtir tout projet
            </p>

          </div>
        </div>

        <div class="row content">
          <div class="col-md-5 order-1 order-md-2" data-aos="fade-left">
            <img src="assets/img/features-2.svg" class="img-fluid" alt="">
          </div>
          <div class="col-md-7 pt-5 order-2 order-md-1" data-aos="fade-right">
            <h3>Innovation continue</h3>
            <p>
                Nous innovons en permanence et vous aidons à vous démarquer
            </p>

          </div>
        </div>

        <div class="row content">
          <div class="col-md-5" data-aos="fade-right">
            <img src="assets/img/features-3.svg" class="img-fluid" alt="">
          </div>
          <div class="col-md-7 pt-5" data-aos="fade-left">
            <h3>Haut niveau de sécurité</h3>
            <p>Nous garantissons une sécurité optimale et la protection de vos données</p>

          </div>
        </div>

      </div>
    </section><!-- End Features Section -->

    <!-- ======= Portfolio Section ======= -->
    <section id="produits" class="portfolio section-bg">
      <div class="container">

        <div class="section-title">
          <h2 data-aos="fade-in">Nos produits</h2>
        </div>

        <div class="row">
          <div class="col-lg-12">
          </div>
        </div>

        <div class="row portfolio-container" data-aos="fade-up">

          <div class="col-lg-4 col-md-6 portfolio-item filter-web">
            <div class="portfolio-wrap">
              <img src="assets/img/produits/oleo.jpeg" class="img-fluid" alt="">
              <div class="portfolio-links">
                <a href="assets/img/produits/oleo.jpeg" data-gall="portfolioGallery" class="venobox" title="Oleo gps"><i class="icofont-plus-circle"></i></a>

                <a href="{{route('oleo')}}" title="plus de details"><i class="icofont-link"></i></a>
              </div>
              <div class="portfolio-info">
                <h4>Oleo gps</h4>
                <p>Solution de tracking</p>
              </div>
            </div>
          </div>
          <div class="col-lg-4 col-md-6 portfolio-item filter-web">
            <div class="portfolio-wrap">
              <img src="assets/img/produits/passafrica.jpeg" class="img-fluid" alt="">
              <div class="portfolio-links">
                <a href="assets/img/produits/passafrica.jpeg" data-gall="portfolioGallery" class="venobox" title="Pass Africa"><i class="icofont-plus-circle"></i></a>
                <a href="{{route('passafrica')}}" title="plus de details"><i class="icofont-link"></i></a>
              </div>
              <div class="portfolio-info">
                <h4>Pass Africa</h4>
                <p>Billeterie </p>
              </div>
            </div>
          </div>
        </div>

      </div>
    </section><!-- End Portfolio Section -->
    <!-- ======= Contact Section ======= -->
    <section id="contact" class="contact section-bg">
      <div class="container">

        <div class="section-title">
          <h2 data-aos="fade-in">Nous contacter</h2>
          <p data-aos="fade-in">
            Voulez-vous prendre contact avec nous?utilisez une de ces références ou laissez nous un message.
        </p>
        </div>

        <div class="row">

          <div class="col-lg-6">

            <div class="row">
              <div class="col-md-12">
                <div class="info-box" data-aos="fade-up">
                  <i class="bx bx-map"></i>
                  <h3>Addresse</h3>
                  <p></p>
                </div>
              </div>
              <div class="col-md-6">
                <div class="info-box mt-4" data-aos="fade-up" data-aos-delay="100">
                  <i class="bx bx-envelope"></i>
                  <h3>Email</h3>
                  <p></p>
                </div>
              </div>
              <div class="col-md-6">
                <div class="info-box mt-4" data-aos="fade-up" data-aos-delay="100">
                  <i class="bx bx-phone-call"></i>
                  <h3>Téléphone</h3>
                  <p></p>
                </div>
              </div>
            </div>

          </div>

          <div class="col-lg-6 mt-4 mt-lg-0">
            <form action="forms/contact.php" method="post" role="form" class="php-email-form w-100" data-aos="fade-up">
              <div class="form-row">
                <div class="col-md-6 form-group">
                  <input type="text" name="name" class="form-control" id="name" placeholder="Votre nom" data-rule="minlen:3" data-msg="votre nom" />
                  <div class="validate"></div>
                </div>
                <div class="col-md-6 form-group">
                  <input type="email" class="form-control" name="email" id="email" placeholder="Votre email" data-rule="email" data-msg="Veuillez entrer un email valide" />
                  <div class="validate"></div>
                </div>
              </div>
              <div class="form-group">
                <input type="text" class="form-control" name="subject" id="subject" placeholder="Objet" data-rule="minlen:4" data-msg="Donnez nous l'objet de votre message" />
                <div class="validate"></div>
              </div>
              <div class="form-group">
                <textarea class="form-control" name="message" rows="5" data-rule="required" data-msg="Laissez nous un message" placeholder="Message"></textarea>
                <div class="validate"></div>
              </div>
              <div class="mb-3">
                <div class="loading">Loading</div>
                <div class="error-message"></div>
                <div class="sent-message">Message envoyé.Merci!</div>
              </div>
              <div class="text-center"><button type="submit">Envoyer le Message</button></div>
            </form>
          </div>

        </div>

      </div>
    </section><!-- End Contact Section -->

  </main><!-- End #main -->

  @include('partials._footer')

  <a href="#" class="back-to-top"><i class="icofont-simple-up"></i></a>


  @include('partials._footerfiles')
</body>

</html>
