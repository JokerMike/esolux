<!DOCTYPE html>
<html lang="fr">
@include('partials._header')
<body>
    <header id="header">
        @include('partials._nav')
    </header>
  <main id="main">
    @yield('content')

  </main>
  @include('partials._footer')
  @include('partials._footerfiles')
</body>
