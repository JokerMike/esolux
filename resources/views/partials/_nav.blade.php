<div class="container d-flex align-items-center">

    <div class="logo mr-auto">
      <h1 class="text-light"><a href="{{route('home')}}">E-SOLUX GROUP</a></h1>

    </div>

    <nav class="nav-menu d-none d-lg-block">
      <ul>
        <li ><a href="{{route('home')}}">Accueil</a></li>
        <li><a href="{{ URL::route('home')}}#about">A propos de nous</a></li>
        <li><a href="{{ URL::route('home')}}#services">Nos services</a></li>
        <li class="drop-down"><a href="{{ URL::route('home')}}#produits">Nos produits</a>
          <ul>
            <li><a href="{{route('oleo')}}">Oleo gps</a></li>
            <li><a href="{{route('passafrica')}}">Pass Africa</a></li>
          </ul>
        </li>
        <li><a href="{{ URL::route('home')}}#contact"> Nous Contacter</a></li>


      </ul>
    </nav>

  </div>
