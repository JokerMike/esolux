<!-- ======= Footer ======= -->
<footer id="footer">

  <div class="footer-top">

    <div class="container" data-aos="fade-up">

      <div class="row  justify-content-center">
        <div class="col-lg-6">
          <h3>Souscrire a notre newsletter</h3>
          <p>Entrer votre email pour etre toujours au courant de nos actualites</p>
        </div>
      </div>

      <div class="row footer-newsletter justify-content-center">
        <div class="col-lg-6">
          <form action="" method="post">
            <input type="email" name="email" placeholder="Entrer votre email"><input type="submit" value="S'abonner">
          </form>
        </div>
      </div>

      <div class="social-links">
          <p class="mb-4"> Nous suivre sur les reseaux sociaux</p>
        <a href="#" class="twitter"><i class="bx bxl-twitter"></i></a>
        <a href="#" class="facebook"><i class="bx bxl-facebook"></i></a>
        <a href="#" class="instagram"><i class="bx bxl-instagram"></i></a>
        <a href="#" class="google-plus"><i class="bx bxl-skype"></i></a>
        <a href="#" class="linkedin"><i class="bx bxl-linkedin"></i></a>
      </div>

    </div>
  </div>

  <div class="container footer-bottom clearfix">
    <div class="copyright">
      &copy; Copyright <strong><span>E-SOLUX GROUP</span></strong>. All Rights Reserved
    </div>

  </div>
</footer><!-- End Footer -->
