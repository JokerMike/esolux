@extends('layout.details')

@section('content')
<section class="breadcrumbs">
    <div class="container">
      <div class="d-flex justify-content-between align-items-center">
        <h2><strong>Oleo gps</strong></h2>
        <ol>
          <li><a href="{{route('home')}}">E-SOLUX GROUP</a></li>
          <li><a href="{{ URL::route('home')}}#produits">Produits</a></li>
          <li>Oloeo gps</li>
        </ol>
      </div>

    </div>
  </section>
<section class="portfolio-details">
  <div class="container">

    <img src=" {{ asset('assets/img/produits/oleo.jpeg') }}" class="img-fluid" alt="">
    <div class="portfolio-details-container">

      <div class="portfolio-info">
        <h3>Informations sur le produit</h3>
        <ul>
          <li><strong>Catégorie</strong>:Tracking et suivi</li>
          <li><strong>Année de lancement</strong>:2020</li>
        </ul>
      </div>

    </div>

    <div class="portfolio-description">
      <h2>OLEO GPS</h2>
      <p>
        OLEO GPS est une solution de géolocalisation gps et gestion de flotte des véhicules développé par la société E-solux Group intégrateur et concepteur des systèmes d’information. Notre solution vous permet la localisation
        et le suivi gps et de connaitre en temps réel la position de votre flotte de véhicule partout au TOGO</p>
        <p>Oleo gps met à votre disposition une plate-forme de géolocalisation WEB et Mobile (Android, IOS…) qui traite les données envoyées par le traceur GPS installé sur lé véhicule en temps réel.
        Ce système de géolocalisation et tracking gps vous offre un accès à des informations importantes sur les véhicules de votre flotte ce qui permet une véritable économie,
        optimisation et efficacité de votre entreprise.Notre solution de géolocalisation vous offre un très large éventail de fonctionnalités.À côté de ces principales fonctions, oleo gps peut ajouter de nombreuses options afin d’obtenir d’autres fonctionnalités sur mesure pour votre entreprise.
        </p>
        <p>
            Nos techniciens s’occupent de la configuration et l’installation du traceur GPS sur votre véhicule chez vous ou sur votre lieu de travail pour vous garantir une meilleur géolocalisation.
        </p>
        <section id="faq" class="faq section-bg">
            <div class="container">
                <div class="text-center">
                    <h3 ><strong>Fonctionnalités fournis par OLEO GPS</strong></h3>
                <p>Oleo gps vous offre un très large éventail de fonctionnalités:</p>
                </div>

              <div class="row faq-item d-flex align-items-stretch" data-aos="fade-up">
                <div class="col-lg-5">

                  <h4>Suivi des vehicules par gps</h4>
                </div>
                <div class="col-lg-7">
                  <p>
                    Vous permet d’analyser les déplacements des véhicules en temps réel et de fournir des données sur la vitesse
                    et l’endroit exacte de chaque véhicule grâce à <span><strong>la géolocalisation</strong></span> gps.</p>
                </div>
              </div>

              <div class="row faq-item d-flex align-items-stretch" data-aos="fade-up" data-aos-delay="100">
                <div class="col-lg-5">

                  <h4>Historique des déplacements</h4>
                </div>
                <div class="col-lg-7">
                    <p>Cette fonctionnalité vous offre la possibilité de visualiser vos historiques de déplacements de chaque véhicule sur les périodes de votre choix.</p>

                </div>
              </div>

              <div class="row faq-item d-flex align-items-stretch" data-aos="fade-up" data-aos-delay="200">
                <div class="col-lg-5">

                  <h4>zones de surveillance</h4>
                </div>
                <div class="col-lg-7">
                    <p>Vous pouvez programmer des zones de surveillance (geofencing) sur la carte et recevoir des alertes lorsque vos véhicules entrent ou sortent de ces zones</p>
                </div>
              </div>

              <div class="row faq-item d-flex align-items-stretch" data-aos="fade-up" data-aos-delay="300">
                <div class="col-lg-5">

                  <h4>Alertes et notifications</h4>
                </div>
                <div class="col-lg-7">
                    <p>Définissez et paramétrez vous-même vos alertes en temps réel selon les événements de votre choix.</p>
                </div>
              </div>

              <div class="row faq-item d-flex align-items-stretch" data-aos="fade-up" data-aos-delay="400">
                <div class="col-lg-5">

                  <h4>Gestion Parc Automobile</h4>
                </div>
                <div class="col-lg-7">
                 <p>Notre solution regroupe toutes les informations nécessaires àla gestion d’une flotte automobile et vous permet une meilleur gestion de votre parc de véhicules.</p>
                </div>
              </div>

              <div class="row faq-item d-flex align-items-stretch" data-aos="fade-up" data-aos-delay="500">
                <div class="col-lg-5">

                  <h4>Gestion du carburant</h4>
                </div>
                <div class="col-lg-7">
                    <p>Oleo gps vous aide à réaliser des économies de carburant et à réduire les coûts vous pouvez mesurer la consommation de carburant de la totalité de vos véhicules</p>
                </div>
              </div>

              <div class="row faq-item d-flex align-items-stretch" data-aos="fade-up" data-aos-delay="600">
                <div class="col-lg-5">

                  <h4>Couper le moteur a distance</h4>
                </div>
                <div class="col-lg-7">
                    <p>En cas de vol de votre véhicule vous pouvez immobilier et couper à distance votre moteur avec un simple clic ou SMS depuis votre téléphone mobile</p>

                </div>
              </div>
              <div class="row faq-item d-flex align-items-stretch" data-aos="fade-up" data-aos-delay="700">
                <div class="col-lg-5">

                  <h4>Véhicules plus proches</h4>
                </div>
                <div class="col-lg-7">
                    <p>Augmentez votre réactivité en localisant les véhicules le plus proches de votre position GPS avec une distance paramétrable</p>

                </div>
              </div>
              <div class="row faq-item d-flex align-items-stretch" data-aos="fade-up" data-aos-delay="800">
                <div class="col-lg-5">

                  <h4>Fonctionnalités sur mesure</h4>
                </div>
                <div class="col-lg-7">
                    <p>Oleo gps étudie toute demande de développement de fonctionnalités sur mesure afin de vous proposer un environnement de travail toujours plus adapté.</p>

                </div>
              </div>

            </div>
          </section>

    </div>
  </div>
</section>
@endsection
