<?php

use Illuminate\Support\Facades\Route;

Route::get('/','ViewController@index')->name('home');
Route::get('/oleogps','ViewController@oleo')->name('oleo');
Route::get('/passafrica','ViewController@passafrica')->name('passafrica');
