<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ViewController extends Controller
{
    public function index(){

        return view('welcome');
    }

    public function oleo(){
        return view('oleo');
    }

    public function passafrica(){
        return view('passafrica');
    }

}
